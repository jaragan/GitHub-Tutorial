/**
 * 
 */
package com.tdd.learn.api;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Nagaraj
 *
 */
public class AssertTests {
	
	public NameValueType[] n1;
	public NameValueType[] n2;
	
	
	@Before
	public void setUp(){
		NameValueType obj1 = new NameValueType("N1","Bangalore");
		NameValueType obj2 = new NameValueType("N2","Mysore");
		
		NameValueType obj3 = new NameValueType("N1","Ballari");
		NameValueType obj4 = new NameValueType("N2","Mangalore");
		
		n1 = new NameValueType[2];
		n2= new NameValueType[2];
		n1[0]=obj1;
		n1[1]=obj2;
		n2[0]=obj3;
		n2[1]=obj4;
	}

	
	/**
	 * Test case for Integer Array
	 */
	@Test
	public void testAssertArrayEquals(){
		int [] expectedVal ={1,2,3};
		int [] actualVal={1,2,3};
		assertArrayEquals(expectedVal, actualVal);
		
	}
	
	/**
	 * Test case for String Array
	 */
	@Test
	public void testAssertArrayEqualsForString(){
		String [] expectedVal ={"A","B"};
		String [] actualVal={"A","B"};
		assertArrayEquals(expectedVal, actualVal);
		
	}
	
	@Test
	public void testAssertArrayEqualsForObject(){
		assertArrayEquals(n1, n2);
	}

}
