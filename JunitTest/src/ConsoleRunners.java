import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;

/**
 * 
 */

/**
 * @author Nagaraj
 *
 */
public class ConsoleRunners {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		

		JUnitCore consoleRunner = new  JUnitCore();
		consoleRunner.addListener(new TextListener(System.out));
		consoleRunner.run(TrackingServiceTest.class);
	}

}
