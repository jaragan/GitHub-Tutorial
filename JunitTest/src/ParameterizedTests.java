import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.junit.tut.TrackingService;


@RunWith(Parameterized.class)
public class ParameterizedTests {

	public static TrackingService service = new TrackingService();
	private int input;
	private int output;
	
	@Parameters
	public static List<Object[]> data(){
		return Arrays.asList(new Object[][]{
			{1,1},
			{-12,0},
			{100,100},
			{1,101}
		});
		
	}

	public ParameterizedTests(int input,int output) {
		super();
		
		this.input = input;
		this.output = output;
		
	}
	
	@Test
	public void test(){
		if(input > 0){
			service.addProteint(input);
		}else{
			service.removeProtein(-input);
		}
		assertEquals(output,service.getTotal());
	}
	
	
	
}
