import static org.junit.Assert.*;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.junit.tut.InvalidGoalException;
import com.junit.tut.TrackingService;

public class TrackingServiceTest {

	private TrackingService service;
	
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("@BeforeClass");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("@AfterClass");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("@Before");
		service = new TrackingService();
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("@After");
	}

	@Test
	public void testTotalProteinVal(){
		assertEquals("Initial Total protein val ", 0, service.getTotal());
	}

	@Test
	public void testWhenProteinIsAdded(){
		service.addProteint(8);
		assertEquals("Added protein val ", 8, service.getTotal());
	}
	
	@Test
	public void testWhenProteinIsRemoved(){
		service.removeProtein(10);
		assertEquals("Removed protein val ", 0, service.getTotal());
	}
	
	@Test
	public void testGoalInvalidException() throws InvalidGoalException{
		thrown.expect(InvalidGoalException.class);
		thrown.expectMessage("Goal was less than zero");
		service.setGoal(-1);
		
	}
	@Test(timeout=200)
	public void testTimoutWhileAddOperation(){
		for(int i=0;i<10000;i++){
			service.addProteint(i);
		}
	}
}
