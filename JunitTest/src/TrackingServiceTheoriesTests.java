


import static org.junit.Assert.assertTrue;

import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import com.junit.tut.TrackingService;

@RunWith(Theories.class)
public class TrackingServiceTheoriesTests {
	
	
	
	@DataPoints
	public static int[] data(){
		return new int[]{1,2,67,2,4};
		
	}

	@Theory
	public void testPositiveProtein(int val){
		TrackingService service = new TrackingService();
		service.addProteint(val);
		
		assertTrue(service.getTotal()>0);
	}
}
