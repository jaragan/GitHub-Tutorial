package com.junit.tut;

public class HistoryItem {
	
	private int historyId;
	private int amount;
	private String operation;
	private int total;

	

	public HistoryItem(int historyId, int amount, String operation, int total) {
		super();
		this.historyId = historyId;
		this.amount = amount;
		this.operation = operation;
		this.total = total;
	}

	public int getHistoryId() {
		return historyId;
	}

	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
