package com.junit.tut;

public class InvalidGoalException extends Exception {

	public InvalidGoalException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
